Option Strict Off
Option Explicit On 
Imports System.Runtime.InteropServices
Imports VB = Microsoft.VisualBasic
Friend Class Master
	Inherits System.Windows.Forms.Form
#Region "Windows フォーム デザイナによって生成されたコード"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'スタートアップ フォームについては、最初に作成されたインスタンスが既定インスタンスになります。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'この呼び出しは、Windows フォーム デザイナで必要です。
		InitializeComponent()
	End Sub
	'Form は、コンポーネント一覧に後処理を実行するために dispose をオーバーライドします。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows フォーム デザイナで必要です。
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents Text_Del As System.Windows.Forms.Button
    Public WithEvents Ret_Del As System.Windows.Forms.Button
	Public WithEvents GpDelimBox As System.Windows.Forms.ComboBox
	Public WithEvents MyAddress As System.Windows.Forms.TextBox
	Public WithEvents YradrBox As System.Windows.Forms.ComboBox
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Frame2 As System.Windows.Forms.GroupBox
	Public WithEvents Send As System.Windows.Forms.Button
	Public WithEvents Receive As System.Windows.Forms.Button
	Public WithEvents ReceiveData As System.Windows.Forms.TextBox
	Public WithEvents SendData As System.Windows.Forms.TextBox
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents Init As System.Windows.Forms.Button
	Public WithEvents Exit_Renamed As System.Windows.Forms.Button
	Public WithEvents Polling As System.Windows.Forms.Button
	Public WithEvents TextRet As System.Windows.Forms.TextBox
	Public WithEvents GetCmd As System.Windows.Forms.Button
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	'メモ : 以下のプロシージャは Windows フォーム デザイナで必要です。
	'Windows フォーム デザイナを使って変更できます。
	'コードエディタを使って修正しないでください。


	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Text_Del = New System.Windows.Forms.Button()
        Me.Ret_Del = New System.Windows.Forms.Button()
        Me.GpDelimBox = New System.Windows.Forms.ComboBox()
        Me.Frame2 = New System.Windows.Forms.GroupBox()
        Me.MyAddress = New System.Windows.Forms.TextBox()
        Me.YradrBox = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Frame1 = New System.Windows.Forms.GroupBox()
        Me.Send = New System.Windows.Forms.Button()
        Me.Receive = New System.Windows.Forms.Button()
        Me.ReceiveData = New System.Windows.Forms.TextBox()
        Me.SendData = New System.Windows.Forms.TextBox()
        Me.Init = New System.Windows.Forms.Button()
        Me.Exit_Renamed = New System.Windows.Forms.Button()
        Me.Polling = New System.Windows.Forms.Button()
        Me.TextRet = New System.Windows.Forms.TextBox()
        Me.GetCmd = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Frame2.SuspendLayout()
        Me.Frame1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Text_Del
        '
        Me.Text_Del.BackColor = System.Drawing.SystemColors.Control
        Me.Text_Del.Cursor = System.Windows.Forms.Cursors.Default
        Me.Text_Del.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Text_Del.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Text_Del.Location = New System.Drawing.Point(289, 152)
        Me.Text_Del.Name = "Text_Del"
        Me.Text_Del.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text_Del.Size = New System.Drawing.Size(116, 33)
        Me.Text_Del.TabIndex = 9
        Me.Text_Del.Text = "データ消去(&D)"
        '
        'Ret_Del
        '
        Me.Ret_Del.BackColor = System.Drawing.SystemColors.Control
        Me.Ret_Del.Cursor = System.Windows.Forms.Cursors.Default
        Me.Ret_Del.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Ret_Del.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Ret_Del.Location = New System.Drawing.Point(171, 152)
        Me.Ret_Del.Name = "Ret_Del"
        Me.Ret_Del.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Ret_Del.Size = New System.Drawing.Size(116, 33)
        Me.Ret_Del.TabIndex = 8
        Me.Ret_Del.Text = "戻り値消去(&C)"
        '
        'GpDelimBox
        '
        Me.GpDelimBox.BackColor = System.Drawing.SystemColors.Window
        Me.GpDelimBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.GpDelimBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.GpDelimBox.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.GpDelimBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GpDelimBox.Items.AddRange(New Object() {"未使用", "CR+LF", "CR", "LF"})
        Me.GpDelimBox.Location = New System.Drawing.Point(416, 80)
        Me.GpDelimBox.Name = "GpDelimBox"
        Me.GpDelimBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.GpDelimBox.Size = New System.Drawing.Size(105, 23)
        Me.GpDelimBox.TabIndex = 19
        '
        'Frame2
        '
        Me.Frame2.BackColor = System.Drawing.SystemColors.Control
        Me.Frame2.Controls.AddRange(New System.Windows.Forms.Control() {Me.MyAddress, Me.YradrBox, Me.Label5, Me.Label4})
        Me.Frame2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Frame2.Location = New System.Drawing.Point(8, 2)
        Me.Frame2.Name = "Frame2"
        Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame2.Size = New System.Drawing.Size(397, 45)
        Me.Frame2.TabIndex = 13
        Me.Frame2.TabStop = False
        '
        'MyAddress
        '
        Me.MyAddress.AcceptsReturn = True
        Me.MyAddress.AutoSize = False
        Me.MyAddress.BackColor = System.Drawing.SystemColors.Window
        Me.MyAddress.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.MyAddress.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.MyAddress.ForeColor = System.Drawing.SystemColors.WindowText
        Me.MyAddress.Location = New System.Drawing.Point(130, 14)
        Me.MyAddress.MaxLength = 0
        Me.MyAddress.Multiline = True
        Me.MyAddress.Name = "MyAddress"
        Me.MyAddress.ReadOnly = True
        Me.MyAddress.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.MyAddress.Size = New System.Drawing.Size(25, 24)
        Me.MyAddress.TabIndex = 15
        Me.MyAddress.TabStop = False
        Me.MyAddress.Text = ""
        Me.MyAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'YradrBox
        '
        Me.YradrBox.BackColor = System.Drawing.SystemColors.Window
        Me.YradrBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.YradrBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.YradrBox.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.YradrBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.YradrBox.Items.AddRange(New Object() {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"})
        Me.YradrBox.Location = New System.Drawing.Point(328, 16)
        Me.YradrBox.Name = "YradrBox"
        Me.YradrBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.YradrBox.Size = New System.Drawing.Size(49, 23)
        Me.YradrBox.TabIndex = 14
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.Control
        Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label5.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(22, 18)
        Me.Label5.Name = "Label5"
        Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label5.Size = New System.Drawing.Size(109, 19)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "マイアドレス ＝ "
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.Control
        Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label4.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(178, 18)
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label4.Size = New System.Drawing.Size(143, 17)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "相手機器アドレス ＝"
        '
        'Frame1
        '
        Me.Frame1.BackColor = System.Drawing.SystemColors.Control
        Me.Frame1.Controls.AddRange(New System.Windows.Forms.Control() {Me.Send, Me.Receive, Me.ReceiveData, Me.SendData})
        Me.Frame1.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Frame1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Frame1.Location = New System.Drawing.Point(8, 52)
        Me.Frame1.Name = "Frame1"
        Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame1.Size = New System.Drawing.Size(397, 93)
        Me.Frame1.TabIndex = 12
        Me.Frame1.TabStop = False
        Me.Frame1.Text = "送受信データ"
        '
        'Send
        '
        Me.Send.BackColor = System.Drawing.SystemColors.Control
        Me.Send.Cursor = System.Windows.Forms.Cursors.Default
        Me.Send.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Send.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Send.Location = New System.Drawing.Point(300, 18)
        Me.Send.Name = "Send"
        Me.Send.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Send.Size = New System.Drawing.Size(85, 31)
        Me.Send.TabIndex = 5
        Me.Send.Text = "送信(&S)"
        '
        'Receive
        '
        Me.Receive.BackColor = System.Drawing.SystemColors.Control
        Me.Receive.Cursor = System.Windows.Forms.Cursors.Default
        Me.Receive.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Receive.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Receive.Location = New System.Drawing.Point(300, 54)
        Me.Receive.Name = "Receive"
        Me.Receive.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Receive.Size = New System.Drawing.Size(85, 31)
        Me.Receive.TabIndex = 7
        Me.Receive.Text = "受信(&R)"
        '
        'ReceiveData
        '
        Me.ReceiveData.AcceptsReturn = True
        Me.ReceiveData.AutoSize = False
        Me.ReceiveData.BackColor = System.Drawing.SystemColors.Window
        Me.ReceiveData.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.ReceiveData.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.ReceiveData.ForeColor = System.Drawing.SystemColors.WindowText
        Me.ReceiveData.Location = New System.Drawing.Point(12, 56)
        Me.ReceiveData.MaxLength = 0
        Me.ReceiveData.Name = "ReceiveData"
        Me.ReceiveData.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ReceiveData.Size = New System.Drawing.Size(281, 25)
        Me.ReceiveData.TabIndex = 6
        Me.ReceiveData.Text = ""
        '
        'SendData
        '
        Me.SendData.AcceptsReturn = True
        Me.SendData.AutoSize = False
        Me.SendData.BackColor = System.Drawing.SystemColors.Window
        Me.SendData.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.SendData.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.SendData.ForeColor = System.Drawing.SystemColors.WindowText
        Me.SendData.Location = New System.Drawing.Point(12, 22)
        Me.SendData.MaxLength = 0
        Me.SendData.Name = "SendData"
        Me.SendData.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SendData.Size = New System.Drawing.Size(281, 25)
        Me.SendData.TabIndex = 4
        Me.SendData.Text = """ABCDEFGHIJK"" Test Data / 35 Bytes "
        '
        'Init
        '
        Me.Init.BackColor = System.Drawing.SystemColors.Control
        Me.Init.Cursor = System.Windows.Forms.Cursors.Default
        Me.Init.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Init.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Init.Location = New System.Drawing.Point(416, 8)
        Me.Init.Name = "Init"
        Me.Init.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Init.Size = New System.Drawing.Size(111, 33)
        Me.Init.TabIndex = 0
        Me.Init.Text = "初期化(&I)"
        '
        'Exit_Renamed
        '
        Me.Exit_Renamed.BackColor = System.Drawing.SystemColors.Control
        Me.Exit_Renamed.Cursor = System.Windows.Forms.Cursors.Default
        Me.Exit_Renamed.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Exit_Renamed.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Exit_Renamed.Location = New System.Drawing.Point(416, 192)
        Me.Exit_Renamed.Name = "Exit_Renamed"
        Me.Exit_Renamed.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Exit_Renamed.Size = New System.Drawing.Size(111, 33)
        Me.Exit_Renamed.TabIndex = 3
        Me.Exit_Renamed.Text = "終了(&X)"
        '
        'Polling
        '
        Me.Polling.BackColor = System.Drawing.SystemColors.Control
        Me.Polling.Cursor = System.Windows.Forms.Cursors.Default
        Me.Polling.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Polling.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Polling.Location = New System.Drawing.Point(416, 112)
        Me.Polling.Name = "Polling"
        Me.Polling.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Polling.Size = New System.Drawing.Size(111, 33)
        Me.Polling.TabIndex = 1
        Me.Polling.Text = "ポーリング(&P)"
        '
        'TextRet
        '
        Me.TextRet.AcceptsReturn = True
        Me.TextRet.AutoSize = False
        Me.TextRet.BackColor = System.Drawing.SystemColors.Window
        Me.TextRet.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TextRet.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextRet.ForeColor = System.Drawing.SystemColors.WindowText
        Me.TextRet.Location = New System.Drawing.Point(8, 192)
        Me.TextRet.MaxLength = 0
        Me.TextRet.Name = "TextRet"
        Me.TextRet.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TextRet.Size = New System.Drawing.Size(393, 25)
        Me.TextRet.TabIndex = 10
        Me.TextRet.Text = ""
        '
        'GetCmd
        '
        Me.GetCmd.BackColor = System.Drawing.SystemColors.Control
        Me.GetCmd.Cursor = System.Windows.Forms.Cursors.Default
        Me.GetCmd.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.GetCmd.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GetCmd.Location = New System.Drawing.Point(416, 152)
        Me.GetCmd.Name = "GetCmd"
        Me.GetCmd.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.GetCmd.Size = New System.Drawing.Size(111, 33)
        Me.GetCmd.TabIndex = 2
        Me.GetCmd.Text = "GET(&G)"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(416, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(73, 17)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "デリミタ"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(4, 168)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(49, 17)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "戻り値"
        '
        'Master
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 12)
        Me.ClientSize = New System.Drawing.Size(534, 231)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.Text_Del, Me.Ret_Del, Me.GpDelimBox, Me.Frame2, Me.Frame1, Me.Init, Me.Exit_Renamed, Me.Polling, Me.TextRet, Me.GetCmd, Me.Label1, Me.Label3})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Location = New System.Drawing.Point(220, 222)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Master"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "マスタモード"
        Me.Frame2.ResumeLayout(False)
        Me.Frame1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "アップグレード ウィザードのサポート コード"
	Private Shared m_vb6FormDefInstance As Master
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As Master
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New Master()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	Dim Mode As Integer								'モードの設定
	Dim SendBuf As String							'送信バッファ
	Dim RecvBuf As New String("", 10000)			'受信バッファ
	Dim Pstb(15) As Integer							'ステータスバイトの配列
	Dim ErrText As String							'テキスト表示用
	Dim Srlen As Integer							'文字列の長さ
	Dim Delim As Integer							'デリミタ
	Dim gChCmd As GCHandle							'Cmd配列用GCHandle
	Dim gChPstb As GCHandle							'Pstb配列用GCHandle
	Dim pPstb As IntPtr								'Pstb配列用IntPtr

	'///// [ フォームロード ] /////////////////////////////////////////////////////////////////
	Private Sub Master_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

		YradrBox.Text = CStr(1)						'相手機器アドレスの初期値を設定します｡
		GpDelimBox.SelectedIndex = 1				'初期設定
		TextRet.Text = ""

		'PinnedハンドルをCmdに分配する
		If gChCmd.IsAllocated = False Then
			gChCmd = GCHandle.Alloc(Cmd, GCHandleType.Pinned)
		End If
		'PinnedハンドルをPstbに分配する
		If gChPstb.IsAllocated = False Then
			gChPstb = GCHandle.Alloc(Pstb, GCHandleType.Pinned)
		End If
		'Pinnedハンドルでオブジェクトのアドレスを取得
		pCmd = gChCmd.AddrOfPinnedObject()
		pPstb = gChPstb.AddrOfPinnedObject()

	End Sub


	Private Sub GpDelimBox_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GpDelimBox.SelectedIndexChanged

		Eoi = 1										'Eoi = 0:使用しない/1:使用する

		Delim = GpDelimBox.SelectedIndex
		Ret = GpDelim(Delim, Eoi)					'デリミタコード(EOI)送出の指定をします。(指定しなくてもいいです｡)

	End Sub


	'///// [ 初期化 ] /////////////////////////////////////////////////////////////////////////
	Private Sub Init_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Init.Click

		Ret = GpibInit(ErrText)						'SubClassにこの関数が入っています。
		If (Ret = 1) Then GoTo Err_Renamed
		Ret = GpBoardsts(&HAS, Mode)

		If Mode = 1 Then
			ErrText = "この機器はマスタではありません｡設定を確認してください｡"
			GoTo Err_Renamed
		End If
		MyAddress.Text = CStr(MyAddr)				'マイアドレスを表示

		If YradrBox.Items.Count = 31 Then			'マイアドレスをリストから削除します。
			YradrBox.Items.RemoveAt((MyAddr))
			If MyAddr = YrAddr Then
				YradrBox.SelectedIndex = 0
			End If
		End If

		Call GpDelimBox_SelectedIndexChanged(GpDelimBox, New System.EventArgs())

Err_Renamed:
		TextRet.Text = ErrText

	End Sub

	'///// [ GET ] ////////////////////////////////////////////////////////////////////////////
	Private Sub GetCmd_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GetCmd.Click

		Ret = GpGet(YrAddr)							'相手機器に対して[Get]コマンドを実行しています｡
		If CheckRet("GpGet", Ret, ErrText) = 1 Then GoTo Err_Renamed
Err_Renamed:
		TextRet.Text = ErrText

	End Sub

	'///// [ ポーリング ] /////////////////////////////////////////////////////////////////////
	Private Sub Polling_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Polling.Click
		TextRet.Text = "ポーリング中です..."
		TextRet.Refresh()												'画面の更新

		For Cnt = 0 To 1												'ポーリングする相手機器の数＋１です｡
			Pstb(Cnt) = 0												'値の初期化をしています｡
		Next

		Cmd(0) = 1														'コマンド総数
		Cmd(1) = YrAddr													'どの機器に対してポーリングするか取得します｡
		Ret = GpPoll(pCmd, pPstb)										'シリアルポールを実行します｡

		If ((Ret And &HFFS) = 0) Or ((Ret And &HFFS) = 1) Then			'SRQが見つかった場合は０か１が返ってきます｡
			ErrText = "SRQを確認｡トーカは ｢" & Cmd(Pstb(0)) & "｣ : ステータスバイトは ｢" & Hex(Pstb(Pstb(0))) & "｣H です｡"
		ElseIf (Ret = 128) Then											'見つからなかった場合は１２８が返ってきます｡
			ErrText = "｢SRQ｣ は見つかりませんでした｡"
		Else
			If CheckRet("GpPoll", Ret, ErrText) = 1 Then GoTo Err_Renamed
		End If

Err_Renamed:
		TextRet.Text = ErrText
	End Sub

	'///// [ 送信 ] ///////////////////////////////////////////////////////////////////////////
	Private Sub Send_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Send.Click
		TextRet.Text = "スレーブが受信するのを待っています．．．"
		TextRet.Refresh()				  '画面の更新

		SendBuf = SendData.Text											'送信データをバッファに入力します｡
		Srlen = Len(SendBuf)											'バッファの大きさを取得します｡
		Cmd(0) = 2														'総コマンド数
		Cmd(1) = MyAddr													'トーカアドレスを決定します｡
		Cmd(2) = YrAddr													'リスナ(相手機器)アドレスを決定します｡

		If Srlen = 0 Then												'送信データが０の時の処理です｡
			TextRet.Text = "送信データがありません｡"
			Exit Sub													'このサブルーチンから抜けます｡
		End If

		Ret = GpTalk(pCmd, Srlen, SendBuf)								'送信を実行します｡
		If CheckRet("GpTalk", Ret, ErrText) = 1 Then GoTo Err_Renamed


Err_Renamed:
		TextRet.Text = ErrText
	End Sub

	'///// [ 受信 ] ///////////////////////////////////////////////////////////////////
	Private Sub Receive_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Receive.Click
		TextRet.Text = "スレーブから受信しています．．．"
		TextRet.Refresh()												'画面の更新

		Srlen = Len(RecvBuf)											'取得する受信の最大値を指定します｡
		Cmd(0) = 2														'総コマンド数
		Cmd(1) = YrAddr													'リスナ(相手機器)アドレスを取得します｡
		Cmd(2) = MyAddr													'トーカアドレスを取得します｡

		Ret = GpListen(pCmd, Srlen, RecvBuf)							'アスキーデータの受信を行います｡
		If CheckRet("GpListen", Ret, ErrText) = 1 Then GoTo Err_Renamed
		ReceiveData.Text = VB.Left(RecvBuf, Srlen)						'受信した文字列を表示します｡

Err_Renamed:
		TextRet.Text = ErrText
	End Sub

	'///// [ 戻り値の消去 ] ///////////////////////////////////////////////////////////////////
	Private Sub Ret_Del_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Ret_Del.Click

		TextRet.Text = ""

	End Sub

	'///// [ 送信データの消去 ] ///////////////////////////////////////////////////////////////
	Private Sub Text_Del_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Text_Del.Click

		ReceiveData.Text = ""
		SendData.Text = ""

	End Sub

	'///// [ 相手機器アドレスの変更 ] /////////////////////////////////////////////////////////
	Private Sub YradrBox_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles YradrBox.SelectedIndexChanged

		YrAddr = CInt(YradrBox.Text)

	End Sub

	'///// [ 終了 ] ///////////////////////////////////////////////////////////////////////////
	Private Sub Exit_Renamed_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Exit_Renamed.Click

		Ret = GpibExit()
		Me.Close()					'このダイアログの終了です｡

	End Sub

	Private Sub Master_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed

		'GCHandleの解放
		If gChCmd.IsAllocated = True Then
			gChCmd.Free()
		End If
		If gChPstb.IsAllocated = True Then
			gChPstb.Free()
		End If

	End Sub
End Class